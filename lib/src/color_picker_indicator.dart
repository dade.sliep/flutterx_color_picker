part of 'color_picker.dart';

class ColorIndicator extends StatelessWidget {
  final Color color;
  final double size;
  final BoxShape shape;

  const ColorIndicator({Key? key, required this.color, required this.size, this.shape = BoxShape.circle})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
      width: size,
      height: size,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(boxShadow: [BoxShadow(color: color.darken(.1), blurRadius: 2)], shape: shape),
      child: CustomPaint(painter: IndicatorPainter(color)));
}

class IndicatorPainter extends CustomPainter {
  final Paint _paint;

  IndicatorPainter(Color color) : _paint = Paint()..color = color;

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Offset.zero & size;
    _drawChess(canvas, rect, 10);
    canvas.drawRect(rect, _paint);
  }

  @override
  bool shouldRepaint(IndicatorPainter oldDelegate) => oldDelegate._paint.color != _paint.color;
}

class ColorFlatIndicator extends StatelessWidget {
  static const double _size = 20;
  static const double _textSize = _size * .85;
  static const BorderRadius _radius = BorderRadius.all(Radius.circular(4));
  final Color? color;
  final GestureTapCallback? onTap;

  const ColorFlatIndicator({Key? key, required this.color, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialStatefulWidget(
      onPressed: onTap,
      builder: (context, states, child) {
        final theme = Theme.of(context);
        final borderColor = theme.brightness.isDark ? Colors.white.withOpacity(.87) : Colors.black87;
        final highlighted = states.pressed || states.hovered;
        final color = this.color;
        return Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.end, children: [
          Container(
            width: ColorFlatIndicator._size,
            height: ColorFlatIndicator._size,
            clipBehavior: Clip.antiAlias,
            margin: const EdgeInsets.all(2),
            decoration: BoxDecoration(
                boxShadow: [BoxShadow(color: color ?? borderColor.negative, blurRadius: highlighted ? 3 : 1)],
                borderRadius: ColorFlatIndicator._radius),
            foregroundDecoration: BoxDecoration(
                border: Border.all(width: .5, color: borderColor), borderRadius: ColorFlatIndicator._radius),
            child: color == null
                ? Transform.rotate(
                    angle: -pi / 4,
                    child: FractionallySizedBox(
                        heightFactor: .085, widthFactor: sqrt2, child: Container(color: borderColor)))
                : CustomPaint(painter: IndicatorPainter(color)),
          ),
          const SizedBox(width: 4),
          Text(color == null ? 'NULL' : '#${color.hexString}',
              style: theme.textTheme.bodyText1!.copyWith(
                  fontSize: ColorFlatIndicator._textSize,
                  fontWeight: FontWeight.normal,
                  decoration: highlighted ? TextDecoration.underline : null)),
        ]);
      });
}
