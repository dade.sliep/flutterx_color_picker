part of 'color_picker.dart';

enum TrackType {
  hue,
  saturation,
  saturationForHSL,
  value,
  lightness,
  red,
  green,
  blue,
  alpha,
}

class ColorPickerSlider extends StatelessWidget {
  static const _thumbFactor = 2 / 3;
  static const _trackFactor = 1 / 3;
  final TrackType trackType;
  final HSVColor color;
  final ValueChanged<HSVColor> onChanged;

  const ColorPickerSlider({
    Key? key,
    required this.trackType,
    required this.color,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => LayoutBuilder(builder: (context, constraints) {
        final box = context.findRenderObject() as RenderBox;
        final thumbSize = constraints.maxHeight * _thumbFactor;
        final trackSize = Size(constraints.maxWidth - thumbSize, constraints.maxHeight * _trackFactor);
        var thumbOffset = thumbSize / 2;
        var thumbColor = Colors.black;
        switch (trackType) {
          case TrackType.hue:
            thumbOffset += trackSize.width * color.hue / 360;
            thumbColor = HSVColor.fromAHSV(1, color.hue, 1, 1).toColor();
            break;
          case TrackType.saturation:
            thumbOffset += trackSize.width * color.saturation;
            thumbColor = HSVColor.fromAHSV(1, color.hue, color.saturation, 1).toColor();
            break;
          case TrackType.saturationForHSL:
            thumbOffset += trackSize.width * _hsvToHsl(color).saturation;
            thumbColor = HSLColor.fromAHSL(1, color.hue, _hsvToHsl(color).saturation, .5).toColor();
            break;
          case TrackType.value:
            thumbOffset += trackSize.width * color.value;
            thumbColor = HSVColor.fromAHSV(1, color.hue, 1, color.value).toColor();
            break;
          case TrackType.lightness:
            thumbOffset += trackSize.width * _hsvToHsl(color).lightness;
            thumbColor = HSLColor.fromAHSL(1, color.hue, 1, _hsvToHsl(color).lightness).toColor();
            break;
          case TrackType.red:
            thumbOffset += trackSize.width * color.toColor().red / 0xff;
            thumbColor = color.toColor().withOpacity(1);
            break;
          case TrackType.green:
            thumbOffset += trackSize.width * color.toColor().green / 0xff;
            thumbColor = color.toColor().withOpacity(1);
            break;
          case TrackType.blue:
            thumbOffset += trackSize.width * color.toColor().blue / 0xff;
            thumbColor = color.toColor().withOpacity(1);
            break;
          case TrackType.alpha:
            thumbOffset += trackSize.width * color.toColor().opacity;
            thumbColor = Colors.black.withOpacity(color.alpha);
            break;
        }
        return CustomMultiChildLayout(delegate: _SliderLayout(trackSize, thumbSize), children: <Widget>[
          LayoutId(
              id: _SliderLayout.track,
              child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                  child: CustomPaint(painter: _TrackPainter(trackType, color)))),
          LayoutId(
              id: _SliderLayout.thumb,
              child: Transform.translate(
                  offset: Offset(thumbOffset, 0), child: CustomPaint(painter: _ThumbPainter(thumbColor)))),
          LayoutId(
              id: _SliderLayout.gestureContainer,
              child: GestureDetector(
                onPanDown: (details) => _slideEvent(box, trackSize.width, thumbSize, details.globalPosition),
                onPanUpdate: (details) => _slideEvent(box, trackSize.width, thumbSize, details.globalPosition),
              )),
        ]);
      });

  void _slideEvent(RenderBox box, double trackWidth, double thumbSize, Offset position) {
    final dx = box.globalToLocal(position).dx - thumbSize / 2;
    final progress = dx.clamp(0, trackWidth) / trackWidth;
    switch (trackType) {
      case TrackType.hue:
        return onChanged(color.withHue(progress * 360));
      case TrackType.saturation:
        return onChanged(color.withSaturation(progress));
      case TrackType.saturationForHSL:
        return onChanged(_hslToHsv(_hsvToHsl(color).withSaturation(progress)));
      case TrackType.value:
        return onChanged(color.withValue(progress));
      case TrackType.lightness:
        return onChanged(_hslToHsv(_hsvToHsl(color).withLightness(progress)));
      case TrackType.red:
        return onChanged(HSVColor.fromColor(color.toColor().withRed((progress * 0xff).round())));
      case TrackType.green:
        return onChanged(HSVColor.fromColor(color.toColor().withGreen((progress * 0xff).round())));
      case TrackType.blue:
        return onChanged(HSVColor.fromColor(color.toColor().withBlue((progress * 0xff).round())));
      case TrackType.alpha:
        return onChanged(color.withAlpha(progress));
    }
  }
}

class _ThumbPainter extends CustomPainter {
  final Path _path = Path();
  final Paint _paint = Paint();
  final Color _color;

  _ThumbPainter(this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(0, size.height / 2);
    final radius = size.height / 2;
    final innerRadius = radius * (ColorPickerSlider._trackFactor / ColorPickerSlider._thumbFactor);
    _drawOuterCircle(canvas, center, radius);
    _drawInnerCircle(canvas, center, radius, innerRadius);
  }

  void _drawOuterCircle(Canvas canvas, Offset center, double radius) {
    _path
      ..reset()
      ..addOval(Rect.fromCircle(center: center.translate(1, -2), radius: radius));
    canvas.drawShadow(_path, _color.withOpacity(.6).darken(.2), 2, true);
    canvas.drawCircle(
        center,
        radius,
        _paint
          ..style = PaintingStyle.fill
          ..color = Colors.white);
  }

  void _drawInnerCircle(Canvas canvas, Offset center, double radius, double innerRadius) {
    if (_color.alpha < 255) _drawInnerChess(canvas, center, radius, innerRadius);
    canvas.drawCircle(
        center,
        innerRadius,
        _paint
          ..style = PaintingStyle.stroke
          ..color = Colors.black26);
    canvas.drawCircle(
        center,
        innerRadius,
        _paint
          ..style = PaintingStyle.fill
          ..color = _color);
  }

  void _drawInnerChess(Canvas canvas, Offset center, double radius, double innerRadius) {
    _path
      ..reset()
      ..addOval(Rect.fromCircle(center: center, radius: innerRadius));
    canvas.clipPath(_path);
    _drawChess(canvas, Rect.fromCenter(center: center, width: radius, height: radius), 4);
  }

  @override
  bool shouldRepaint(_ThumbPainter oldDelegate) => oldDelegate._color != _color;
}

class _SliderLayout extends MultiChildLayoutDelegate {
  static const String track = 'track';
  static const String thumb = 'thumb';
  static const String gestureContainer = 'gesture_container';
  final Size trackSize;
  final double thumbSize;

  _SliderLayout(this.trackSize, this.thumbSize);

  @override
  void performLayout(Size size) {
    layoutChild(track, BoxConstraints.tight(trackSize));
    positionChild(track, Offset(thumbSize / 2, (size.height - trackSize.height) / 2));
    layoutChild(thumb, BoxConstraints.tight(Size.square(thumbSize)));
    positionChild(thumb, Offset(0, (size.height - thumbSize) / 2));
    layoutChild(gestureContainer, BoxConstraints.tight(size));
    positionChild(gestureContainer, Offset.zero);
  }

  @override
  bool shouldRelayout(_SliderLayout oldDelegate) => false;
}

class _TrackPainter extends CustomPainter {
  final Paint _paint = Paint();
  final TrackType _trackType;
  final HSVColor _color;

  _TrackPainter(this._trackType, this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Offset.zero & size;
    List<Color> _colors;
    switch (_trackType) {
      case TrackType.hue:
        _colors = <Color>[
          const HSVColor.fromAHSV(1, 0, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 60, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 120, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 180, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 240, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 300, 1, 1).toColor(),
          const HSVColor.fromAHSV(1, 360, 1, 1).toColor(),
        ];
        break;
      case TrackType.saturation:
        _colors = <Color>[
          HSVColor.fromAHSV(1, _color.hue, 0, 1).toColor(),
          HSVColor.fromAHSV(1, _color.hue, 1, 1).toColor(),
        ];
        break;
      case TrackType.saturationForHSL:
        _colors = <Color>[
          HSLColor.fromAHSL(1, _color.hue, 0, .5).toColor(),
          HSLColor.fromAHSL(1, _color.hue, 1, .5).toColor(),
        ];
        break;
      case TrackType.value:
        _colors = <Color>[
          HSVColor.fromAHSV(1, _color.hue, 1, 0).toColor(),
          HSVColor.fromAHSV(1, _color.hue, 1, 1).toColor(),
        ];
        break;
      case TrackType.lightness:
        _colors = <Color>[
          HSLColor.fromAHSL(1, _color.hue, 1, 0).toColor(),
          HSLColor.fromAHSL(1, _color.hue, 1, .5).toColor(),
          HSLColor.fromAHSL(1, _color.hue, 1, 1).toColor(),
        ];
        break;
      case TrackType.red:
        _colors = <Color>[
          _color.toColor().withRed(0).withOpacity(1),
          _color.toColor().withRed(255).withOpacity(1),
        ];
        break;
      case TrackType.green:
        _colors = <Color>[
          _color.toColor().withGreen(0).withOpacity(1),
          _color.toColor().withGreen(255).withOpacity(1),
        ];
        break;
      case TrackType.blue:
        _colors = <Color>[
          _color.toColor().withBlue(0).withOpacity(1),
          _color.toColor().withBlue(255).withOpacity(1),
        ];
        break;
      case TrackType.alpha:
        _drawChess(canvas, rect, 2);
        _colors = <Color>[
          Colors.black.withOpacity(0),
          Colors.black.withOpacity(1),
        ];
        break;
      default:
        return;
    }
    canvas.drawRect(rect, _paint..shader = LinearGradient(colors: _colors).createShader(rect));
  }

  @override
  bool shouldRepaint(_TrackPainter oldDelegate) => oldDelegate._trackType != _trackType || oldDelegate._color != _color;
}
