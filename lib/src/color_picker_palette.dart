part of 'color_picker.dart';

class _ColorPickerPalette extends StatelessWidget {
  final Color _color;
  final ValueChanged<HSVColor> _onChanged;
  final PaletteMode _mode;

  const _ColorPickerPalette(this._color, this._onChanged, this._mode);

  @override
  Widget build(BuildContext context) {
    final swatches = _mode.swatches;
    final shades = _mode.shades.reversed.toList(growable: false);
    final count = shades.length;
    final opaqueColor = _color.withAlpha(255);
    return Column(children: [
      Row(children: shades.map((shade) => _indicatorItem(context, shade)).toList(growable: false)),
      const SizedBox(height: 8),
      Expanded(
        child: GridView.builder(
            itemCount: swatches.length * count,
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: count),
            itemBuilder: (context, index) {
              final swatch = swatches[index ~/ count];
              final color = swatch[shades[index % count]];
              return _buildItem(context, color, color == opaqueColor, count);
            }),
      )
    ]);
  }

  Widget _indicatorItem(BuildContext context, int value) =>
      Expanded(child: Center(child: Text(value.toString(), style: Theme.of(context).textTheme.caption)));

  Widget _buildItem(BuildContext context, Color color, bool isSelected, int count) => MaterialStatefulWidget(
      onPressed: () => _onChanged(HSVColor.fromColor(color.withAlpha(_color.alpha))),
      builder: (context, states, child) => AnimatedContainer(
          key: ValueKey(color),
          margin: const EdgeInsets.all(.5),
          duration: const Duration(milliseconds: 250),
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(isSelected
                  ? 512 / count
                  : states.pressed
                      ? 96 / count
                      : 0)))));
}

enum PaletteMode { primary, accent }

extension PaletteModeExt on PaletteMode {
  List<int> get shades {
    switch (this) {
      case PaletteMode.primary:
        return const [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
      case PaletteMode.accent:
        return const [100, 200, 400, 700];
      default:
        throw UnsupportedError('missing switch branch');
    }
  }

  dynamic get swatches {
    switch (this) {
      case PaletteMode.primary:
        return const [
          Colors.red,
          Colors.pink,
          Colors.purple,
          Colors.deepPurple,
          Colors.indigo,
          Colors.blue,
          Colors.lightBlue,
          Colors.cyan,
          Colors.teal,
          Colors.green,
          Colors.lightGreen,
          Colors.lime,
          Colors.yellow,
          Colors.amber,
          Colors.orange,
          Colors.deepOrange,
          Colors.brown,
          Colors.grey,
          Colors.blueGrey,
        ];
      case PaletteMode.accent:
        return const [
          Colors.redAccent,
          Colors.pinkAccent,
          Colors.purpleAccent,
          Colors.deepPurpleAccent,
          Colors.indigoAccent,
          Colors.blueAccent,
          Colors.lightBlueAccent,
          Colors.cyanAccent,
          Colors.tealAccent,
          Colors.greenAccent,
          Colors.lightGreenAccent,
          Colors.limeAccent,
          Colors.yellowAccent,
          Colors.amberAccent,
          Colors.orangeAccent,
          Colors.deepOrangeAccent,
        ];
      default:
        throw UnsupportedError('missing switch branch');
    }
  }
}
