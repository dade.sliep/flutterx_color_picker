part of 'color_picker.dart';

class _ColorPickerArea extends StatelessWidget {
  final HSVColor _color;
  final ValueChanged<HSVColor> _onChanged;

  const _ColorPickerArea(this._color, this._onChanged);

  @override
  Widget build(BuildContext context) => LayoutBuilder(builder: (context, constraints) {
        final width = constraints.maxWidth;
        final height = constraints.maxHeight;
        return GestureDetector(
            onPanDown: (details) => _handleGesture(details.globalPosition, context, height, width),
            onPanUpdate: (details) => _handleGesture(details.globalPosition, context, height, width),
            child: CustomPaint(painter: _ColorAreaPainter(_color)));
      });

  void _handleGesture(Offset position, BuildContext context, double height, double width) {
    final getBox = context.findRenderObject() as RenderBox;
    final localOffset = getBox.globalToLocal(position);
    final x = localOffset.dx.clamp(0.0, width) / width;
    final y = 1 - localOffset.dy.clamp(0.0, height) / height;
    _onChanged(_color.withSaturation(x).withValue(y));
  }
}

class _ColorAreaPainter extends CustomPainter {
  static const LinearGradient gradientV =
      LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Colors.white, Colors.black]);
  final Paint _paintV = Paint();
  final Paint _paintH = Paint()..blendMode = BlendMode.multiply;
  final Paint _paintPointer = Paint()
    ..strokeWidth = 1
    ..style = PaintingStyle.stroke;
  final HSVColor _color;

  _ColorAreaPainter(this._color);

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Offset.zero & size;
    final gradientH = LinearGradient(colors: [Colors.white, HSVColor.fromAHSV(1, _color.hue, 1, 1).toColor()]);
    canvas.drawRect(rect, _paintV..shader = gradientV.createShader(rect));
    canvas.drawRect(rect, _paintH..shader = gradientH.createShader(rect));
    final center = Offset(size.width * _color.saturation, size.height * (1 - _color.value));
    _paintPointer.color = _color.toColor().brightness.isDark ? Colors.white : Colors.black;
    canvas.drawCircle(center, size.height * 0.04, _paintPointer);
    canvas.drawCircle(center, .5, _paintPointer);
  }

  @override
  bool shouldRepaint(_ColorAreaPainter oldDelegate) => oldDelegate._color != _color;
}
