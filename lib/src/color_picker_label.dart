part of 'color_picker.dart';

class ColorPickerLabel extends StatefulWidget {
  final HSVColor color;
  final ValueChanged<HSVColor> onChanged;

  const ColorPickerLabel({Key? key, required this.color, required this.onChanged}) : super(key: key);

  @override
  State<ColorPickerLabel> createState() => _ColorPickerLabelState();
}

class _ColorPickerLabelState extends State<ColorPickerLabel> with StateObserver {
  final List<TextEditingController> _nodes = List.generate(5, (_) => TextEditingController());
  _AlphaModel _alphaModel = _AlphaModel.values.first;
  _ColorModel _colorModel = _ColorModel.values.first;

  @override
  void registerObservers(BuildContext context) {
    final application = Application.of(context);
    if (application != null)
      observe<bool>(application.isInputMethodVisible, (value) => value ? null : FocusScope.of(context).unfocus());
  }

  @override
  Widget build(BuildContext context) {
    final data = _ColorData(widget.color, _alphaModel, _colorModel);
    return Column(children: [
      Row(children: [
        _labelGroup(
            data: data,
            indexes: [0],
            flex: 1,
            onSwitch: () => setState(() => _alphaModel = _AlphaModel.values.nextElement(_alphaModel))),
        _labelGroup(
            data: data,
            indexes: [1, 2, 3],
            flex: 3,
            onSwitch: () => setState(() => _colorModel = _ColorModel.values.nextElement(_colorModel))),
        _labelGroup(data: data, indexes: [4], flex: 2),
      ]),
    ]);
  }

  Widget _labelGroup({
    required _ColorData data,
    required List<int> indexes,
    required int flex,
    VoidCallback? onSwitch,
  }) =>
      Expanded(
          flex: flex,
          child: Column(children: [
            _PressableContent(
                onTap: onSwitch,
                child: Row(
                    children: indexes
                        .map((index) => Expanded(child: Center(child: _buildLabel(data.labels[index]))))
                        .toList(growable: false))),
            const SizedBox(height: 4),
            Row(
                children: indexes
                    .map((index) => Expanded(child: Center(child: _buildValue(data, index))))
                    .toList(growable: false)),
          ]));

  Widget _buildLabel(String label) => Text(label, style: Theme.of(context).textTheme.bodyText1);

  Widget _buildValue(_ColorData data, int index) {
    final format = data.formats[index];
    final theme = Theme.of(context);
    final text = format.format(data.values[index]);
    final textStyle = theme.textTheme.bodyText1!.copyWith(fontSize: 14, fontWeight: FontWeight.normal);
    final isHexEditor = index == 4;
    return SizedBox(
      width: isHexEditor ? 80 : 40,
      child: TextField(
          textAlign: format.prefix == null ? TextAlign.end : TextAlign.start,
          inputFormatters: [LengthLimitingTextInputFormatter(format.length)],
          controller: _nodes[index]..text = text,
          keyboardType: isHexEditor ? TextInputType.text : TextInputType.number,
          textCapitalization: TextCapitalization.characters,
          autocorrect: false,
          decoration: InputDecoration(
            isDense: true,
            prefixText: format.prefix,
            suffixText: format.suffix,
            prefixStyle: textStyle,
            suffixStyle: textStyle,
            contentPadding: const EdgeInsets.symmetric(horizontal: 2, vertical: 4),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: textStyle.color!.withOpacity(.8), width: .5),
                borderRadius: const BorderRadius.all(Radius.circular(2))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: theme.colorScheme.secondary),
                borderRadius: const BorderRadius.all(Radius.circular(2))),
          ),
          onSubmitted: (value) => widget.onChanged(data.update(widget.color, value, index, _alphaModel, _colorModel)),
          style: textStyle),
    );
  }
}

enum _ColorModel { rgb, hsv, hsl }

enum _AlphaModel { a, o }

class _ColorData {
  final List<String> labels;
  final List<double> values;
  final List<_Format> formats;

  const _ColorData._(this.labels, this.values, this.formats);

  factory _ColorData(HSVColor color, _AlphaModel alphaModel, _ColorModel colorModel) => _ColorData._(
      _labels(alphaModel, colorModel).toList(growable: false),
      _values(color, colorModel).toList(growable: false),
      _format(alphaModel, colorModel).toList(growable: false));

  static Iterable<String> _labels(_AlphaModel alphaModel, _ColorModel colorModel) sync* {
    switch (alphaModel) {
      case _AlphaModel.a:
        yield 'A';
        break;
      case _AlphaModel.o:
        yield 'O';
        break;
      default:
        throw UnsupportedError('missing switch branch');
    }
    switch (colorModel) {
      case _ColorModel.rgb:
        yield* ['R', 'G', 'B'];
        break;
      case _ColorModel.hsv:
        yield* ['H', 'S', 'V'];
        break;
      case _ColorModel.hsl:
        yield* ['H', 'S', 'L'];
        break;
      default:
        throw UnsupportedError('missing switch branch');
    }
    yield 'HEX';
  }

  static Iterable<double> _values(HSVColor hsvColor, _ColorModel colorModel) sync* {
    switch (colorModel) {
      case _ColorModel.rgb:
        final color = hsvColor.toColor();
        yield* [color.opacity * 100, color.red.toDouble(), color.green.toDouble(), color.blue.toDouble()];
        break;
      case _ColorModel.hsv:
        yield* [hsvColor.alpha * 100, hsvColor.hue % 360, hsvColor.saturation * 100, hsvColor.value * 100];
        break;
      case _ColorModel.hsl:
        final hslColor = _hsvToHsl(hsvColor);
        yield* [hsvColor.alpha * 100, hslColor.hue % 360, hslColor.saturation * 100, hslColor.lightness * 100];
        break;
      default:
        throw UnsupportedError('missing switch branch');
    }
    yield hsvColor.toColor().value.toDouble();
  }

  static Iterable<_Format> _format(_AlphaModel alphaModel, _ColorModel colorModel) sync* {
    switch (alphaModel) {
      case _AlphaModel.a:
        yield _Format(length: 3, transform: (value) => _round((value / 100) * 255));
        break;
      case _AlphaModel.o:
        yield const _Format(length: 3, suffix: '%', transform: _round);
        break;
      default:
        throw UnsupportedError('missing switch branch');
    }
    switch (colorModel) {
      case _ColorModel.rgb:
        yield* const [
          _Format(length: 3, transform: _round),
          _Format(length: 3, transform: _round),
          _Format(length: 3, transform: _round)
        ];
        break;
      case _ColorModel.hsv:
        yield* const [
          _Format(length: 3, suffix: '°', transform: _round),
          _Format(length: 3, suffix: '%', transform: _round),
          _Format(length: 3, suffix: '%', transform: _round)
        ];
        break;
      case _ColorModel.hsl:
        yield* const [
          _Format(length: 3, suffix: '°', transform: _round),
          _Format(length: 3, suffix: '%', transform: _round),
          _Format(length: 3, suffix: '%', transform: _round)
        ];
        break;
      default:
        throw UnsupportedError('missing switch branch');
    }

    yield _Format(
      length: 8,
      prefix: '#',
      transform: (value) => Color(_round(value)).hexString,
    );
  }

  HSVColor update(HSVColor color, String value, int index, _AlphaModel alphaModel, _ColorModel colorModel) {
    if (index == 4)
      return HSVColor.fromColor(Color(int.tryParse(value.length == 6 ? 'FF$value' : value, radix: 16) ?? 0));
    final v = int.tryParse(value) ?? 0;
    if (index == 0)
      switch (alphaModel) {
        case _AlphaModel.a:
          return HSVColor.fromColor(color.toColor().withAlpha(v));
        case _AlphaModel.o:
          return color.withAlpha(v / 100.0);
      }
    switch (colorModel) {
      case _ColorModel.rgb:
        switch (index) {
          case 1:
            return HSVColor.fromColor(color.toColor().withRed(v));
          case 2:
            return HSVColor.fromColor(color.toColor().withGreen(v));
          case 3:
            return HSVColor.fromColor(color.toColor().withBlue(v));
        }
        throw UnsupportedError('missing switch branch');
      case _ColorModel.hsv:
        switch (index) {
          case 1:
            return color.withHue(v.toDouble());
          case 2:
            return color.withSaturation(v / 100.0);
          case 3:
            return color.withValue(v / 100.0);
        }
        throw UnsupportedError('missing switch branch');
      case _ColorModel.hsl:
        final hslColor = _hsvToHsl(color);
        switch (index) {
          case 1:
            return _hslToHsv(hslColor.withHue(v.toDouble()));
          case 2:
            return _hslToHsv(hslColor.withSaturation(v / 100.0));
          case 3:
            return _hslToHsv(hslColor.withLightness(v / 100.0));
        }
        throw UnsupportedError('missing switch branch');
    }
  }

  static int _round(double value) => value.round();
}

class _Format {
  final int length;
  final String? prefix;
  final String? suffix;
  final Object Function(double)? transform;

  const _Format({required this.length, this.prefix, this.suffix, this.transform});

  String format(double value) => (transform?.call(value) ?? value).toString();
}

class _PressableContent extends StatefulWidget {
  final VoidCallback? onTap;
  final Widget child;

  const _PressableContent({Key? key, required this.onTap, required this.child}) : super(key: key);

  @override
  _PressableContentState createState() => _PressableContentState();
}

class _PressableContentState extends State<_PressableContent> {
  @override
  Widget build(BuildContext context) => InkWell(
      borderRadius: const BorderRadius.all(Radius.circular(4)),
      onTap: widget.onTap,
      child: Padding(padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2), child: widget.child));
}

class ColorfulIcon extends StatefulWidget {
  final Color color;
  final Color shineColor;
  final Widget icon;
  final FutureOr<bool> Function() onPressed;

  const ColorfulIcon({
    Key? key,
    required this.color,
    required this.shineColor,
    required this.icon,
    required this.onPressed,
  }) : super(key: key);

  @override
  State<ColorfulIcon> createState() => _ColorfulIconState();
}

class _ColorfulIconState extends State<ColorfulIcon> with TickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200), reverseDuration: const Duration(milliseconds: 400));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => AnimatedBuilder(
      animation: _controller,
      builder: (context, child) => IconButton(
          onPressed: () async {
            final result = await widget.onPressed();
            if (result) {
              await _controller.forward();
              await _controller.reverse();
            }
          },
          splashRadius: 24,
          iconSize: 20,
          color: Color.lerp(widget.color, widget.shineColor, _controller.value),
          icon: widget.icon));
}
