import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutter/services.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_live_data/flutterx_live_data.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

part 'color_picker_area.dart';
part 'color_picker_indicator.dart';
part 'color_picker_label.dart';
part 'color_picker_names.dart';
part 'color_picker_palette.dart';
part 'color_picker_slider.dart';

class ColorPicker extends StatefulWidget {
  static final DialogRoute<Color?> route = DialogRoute.alertBuilder(ColorPicker,
      contentPadding: EdgeInsets.zero,
      insetPadding: const EdgeInsets.symmetric(horizontal: 24),
      buttonPadding: const EdgeInsets.symmetric(horizontal: 8),
      clipBehavior: Clip.antiAlias,
      positiveAction: AlertDialogAction<Color?>(onAction: (context, outArgs) => outArgs['result']),
      negativeAction: AlertDialogAction<Color?>(onAction: (context, outArgs) => null),
      builder: (context, args, outArgs) => SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ColorPicker(
              tracks: args['tracks'],
              initialValue: args['initialValue'],
              onChanged: (value) => outArgs['result'] = value)));
  final Set<TrackType> tracks;
  final Color? initialValue;
  final ValueChanged<Color> onChanged;

  const ColorPicker({
    Key? key,
    this.tracks = const {TrackType.hue, TrackType.alpha},
    required this.initialValue,
    required this.onChanged,
  }) : super(key: key);

  static Future<Color?> open({
    required BuildContext context,
    Set<TrackType> tracks = const {TrackType.hue, TrackType.alpha},
    required Color? initialValue,
  }) async =>
      route.open(context, args: {'tracks': tracks, 'initialValue': initialValue});

  @override
  State<ColorPicker> createState() => _ColorPickerState();
}

class _ColorPickerState extends State<ColorPicker> {
  static const double _sliderHeight = 32;
  HSVColor _color = const HSVColor.fromAHSV(1, 0, 0, 0);
  bool _palette = false;
  PaletteMode _paletteMode = PaletteMode.values.first;

  @override
  void initState() {
    super.initState();
    widget.initialValue?.let((value) => _color = HSVColor.fromColor(value));
  }

  @override
  Widget build(BuildContext context) => LayoutBuilder(builder: _palette ? _buildPalette : _buildPicker);

  Widget _buildPicker(BuildContext context, BoxConstraints constraints) {
    final color = _color.toColor();
    final theme = Theme.of(context);
    final pickerArea = _ColorPickerArea(_color, _update);
    final pickerTools = Column(mainAxisSize: MainAxisSize.min, children: [
      _header(color, theme),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              ColorIndicator(color: color, size: 52),
              const SizedBox(width: 12),
              Expanded(
                  child: Column(
                      mainAxisSize: MainAxisSize.min, children: widget.tracks.map(_track).toList(growable: false))),
            ]),
            const SizedBox(height: 12),
            ColorPickerLabel(color: _color, onChanged: _update),
          ])),
    ]);
    return OrientationBuilder(
        builder: (context, orientation) => orientation == Orientation.landscape
            ? Row(children: [
                SizedBox(width: constraints.maxWidth * .4, height: constraints.maxHeight, child: pickerArea),
                SizedBox(
                    width: constraints.maxWidth * .6,
                    height: constraints.maxHeight,
                    child: SingleChildScrollView(
                        padding: EdgeInsets.only(bottom: constraints.maxHeight * .2), child: pickerTools)),
              ])
            : Column(mainAxisSize: MainAxisSize.min, children: [
                SizedBox(
                    width: constraints.maxWidth,
                    height: min(constraints.maxWidth * .65, constraints.maxHeight * .35),
                    child: pickerArea),
                pickerTools,
              ]));
  }

  Widget _buildPalette(BuildContext context, BoxConstraints constraints) {
    final color = _color.toColor();
    final theme = Theme.of(context);
    return Column(mainAxisSize: MainAxisSize.min, children: [
      _header(color, theme),
      SizedBox(height: min(constraints.maxHeight * .8, 400), child: _ColorPickerPalette(color, _update, _paletteMode)),
    ]);
  }

  Widget _header(Color color, ThemeData theme) {
    final affinity = findAffinity(color);
    return Row(children: [
      const SizedBox(width: 16),
      Expanded(
          child: MaterialStatefulWidget(
              onPressed: () => _update(HSVColor.fromColor(affinity.color)),
              builder: (context, states, child) => Text('${affinity.exact ? '' : '~'}${affinity.name}',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: theme.textTheme.bodyText1!
                      .copyWith(decoration: states.pressed || states.hovered ? TextDecoration.underline : null)))),
      ..._actions(color, theme),
    ]);
  }

  Iterable<Widget> _actions(Color color, ThemeData theme) sync* {
    final mainColor = theme.textTheme.bodyText1!.color!;
    if (_palette) {
      yield SizedBox(
          height: 28,
          child: ToggleButtons(
              onPressed: (index) => setState(() => _paletteMode = PaletteMode.values[index]),
              selectedColor: mainColor,
              isSelected: PaletteMode.values.map((value) => value == _paletteMode).toList(growable: false),
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              borderColor: mainColor.withOpacity(.4),
              selectedBorderColor: mainColor,
              fillColor: Colors.transparent,
              children: ['Material', 'Accent']
                  .map((value) => Padding(padding: const EdgeInsets.symmetric(horizontal: 4), child: Text(value)))
                  .toList(growable: false)));
    } else {
      yield ColorfulIcon(
          color: mainColor,
          shineColor: color,
          icon: const Icon(Icons.copy),
          onPressed: () {
            Clipboard.setData(ClipboardData(text: '#${color.hexString}'));
            return true;
          });
      yield ColorfulIcon(
          color: mainColor,
          shineColor: color,
          icon: const Icon(Icons.paste),
          onPressed: () async {
            final data = await Clipboard.getData(Clipboard.kTextPlain);
            final value = int.tryParse(data?.text?.replaceAll('#', '') ?? '', radix: 16);
            if (value == null) return false;
            _update(HSVColor.fromColor(Color(value)));
            return true;
          });
    }
    yield ColorfulIcon(
        color: mainColor,
        shineColor: color,
        icon: _palette ? const Icon(Icons.colorize) : const Icon(Icons.color_lens_outlined),
        onPressed: () {
          setState(() => _palette = !_palette);
          return true;
        });
  }

  Widget _track(TrackType type) => SizedBox(
      height: _sliderHeight,
      width: double.infinity,
      child: ColorPickerSlider(trackType: type, color: _color, onChanged: _update));

  void _update(HSVColor color) {
    setState(() => _color = color);
    widget.onChanged(color.toColor());
  }
}

final Paint _cellPaint = Paint();

void _drawChess(Canvas canvas, Rect rect, double verticalDivisions) {
  rect = rect.deflate(.2);
  final chessSize = Size.square(rect.height / verticalDivisions);
  final yCount = rect.height / chessSize.height;
  final xCount = rect.width / chessSize.width;
  final lastY = yCount.truncate();
  final lastX = xCount.truncate();
  for (var y = 0; y < yCount; y++) {
    for (var x = 0; x < xCount; x++) {
      var size = chessSize;
      if (y == lastY) size = Size(size.width, size.height * (yCount - lastY));
      if (x == lastX) size = Size(size.width * (xCount - lastX), size.height);
      final cell = rect.topLeft.translate(chessSize.width * x, chessSize.height * y) & size;
      _cellPaint.color = (x + y).isOdd ? Colors.white : Colors.black26;
      canvas.drawRect(cell, _cellPaint);
    }
  }
}

HSLColor _hsvToHsl(HSVColor color) {
  final l = (2 - color.saturation) * color.value / 2;
  final s = l == 0.0 || l == 1.0 ? 0.0 : color.saturation * color.value / (l < .5 ? l * 2 : 2 - l * 2);
  return HSLColor.fromAHSL(color.alpha, color.hue, s.clamp(0.0, 1.0), l.clamp(0.0, 1.0));
}

HSVColor _hslToHsv(HSLColor color) {
  final v = color.lightness + color.saturation * (color.lightness < 0.5 ? color.lightness : 1 - color.lightness);
  final s = v == 0.0 ? 0.0 : 2 - 2 * color.lightness / v;
  return HSVColor.fromAHSV(color.alpha, color.hue, s.clamp(0.0, 1.0), v.clamp(0.0, 1.0));
}
