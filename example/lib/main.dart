import 'package:flutter/material.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_color_picker/flutterx_color_picker.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

void main() => runMaterialApp(
    name: 'Flutterx Color Picker Demo',
    routes: {HomeActivity.route},
    materialAppFactory: (key, initialRoute, onGenerateRoute, title, locale, localizationsDelegates, supportedLocales) =>
        MaterialApp(
            key: key,
            initialRoute: initialRoute,
            onGenerateRoute: onGenerateRoute,
            title: title,
            theme: _appTheme,
            locale: locale,
            localizationsDelegates: localizationsDelegates,
            supportedLocales: supportedLocales,
            debugShowCheckedModeBanner: false));

final _appTheme = ThemeData(
    primaryColor: Colors.grey[900],
    colorScheme: ColorScheme.fromSwatch(
        primarySwatch: MaterialColor(
      Colors.grey[900]!.value,
      <int, Color>{
        50: Colors.grey[600]!,
        100: Colors.grey[700]!,
        200: Colors.grey[800]!,
        300: Colors.grey[850]!,
        350: Colors.grey[900]!,
        400: Colors.grey[900]!,
        500: Colors.grey[900]!,
        600: Colors.grey[900]!,
        700: Colors.grey[900]!,
        800: Colors.grey[900]!,
        850: Colors.grey[900]!,
        900: Colors.grey[900]!,
      },
    )));

class HomeActivity extends StatefulWidget {
  static final ActivityRoute<void> route =
      ActivityRoute.builder(HomeActivity, builder: (context, args) => const HomeActivity._());

  const HomeActivity._({Key? key}) : super(key: key);

  @override
  State<HomeActivity> createState() => _HomeActivityState();
}

class _HomeActivityState extends State<HomeActivity> {
  Color _color = Colors.white;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text(HomeActivity.route.name)),
      body: Stack(children: [
        ColorIndicator(color: _color, size: double.infinity, shape: BoxShape.rectangle),
        Center(
            child: Text(findAffinity(_color).toString(),
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(color: _color.brightness.isDark ? Colors.white : Colors.black))),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () => ColorPicker.open(context: context, initialValue: _color)
            .then((value) => value == null ? null : setState(() => _color = value)),
        child: const Icon(Icons.color_lens_outlined),
      ));
}
