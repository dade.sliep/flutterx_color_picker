# flutterx_color_picker

Material color picker inspired by chrome devtools

## Import

Import the library like this:

```dart
import 'package:flutterx_color_picker/flutterx_color_picker.dart';
```

## Usage

Check the documentation in the desired source file of this library