## 1.0.0-dev

* Initial release.

## 1.0.1-dev

* Improve color affinity.

## 1.0.2-dev

* Improve application integration.

## 1.0.3-dev

* Improve UI.